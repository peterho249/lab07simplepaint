# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
**Version 1**
* Choose type of shape from menu.
* Draw line, rectangle, ellipse by left mouse button.
* Draw square, circle by left mouse button and shift.
* Preview shape before draw using factory/ prototype design pattern.
* Save and open paint data from binary file (extension .mpd).

**Version 2**
* Change menu to ribbon.
* Draw shape with GDI+

**Version 3**
* Save and load data function is moved to run-time DLL. DLL is called in OnCommand function.

## Application Flow ##
### Main Flow ###
* Choose shape to draw from menu Shape, drag left button mouse cursor on client to see preview, release left button to draw it.
* Choose Shape->Rectangle, drag mouse cursor and press shift to draw square.
* Choose Shape->Ellipse, drag mouse cursor and press shift to draw circle.
* Choose File->Save and choose folder, type the name to save file.
* Choose File->Open and choose folder, select file has extension mpd to open file.

**Version 2**
* Choose function Open, Save, Exit by clicking on ribbon.
* Choose shape to draw on ribbon.

## BitBucket Link ##
* https://peterho249@bitbucket.org/peterho249/lab07simplepaint.git

## Youtube Link ##
* 