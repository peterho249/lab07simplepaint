// SimplePaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "SimplePaint.h"
#include "CommandHandler.h"

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

HWND g_hWnd;

OPENFILENAME ofn;
WCHAR szFile[BUFFER_SIZE];
vector<CShape*> g_Shapes;
vector<CShape*> g_ShapeModels;
BOOL IsDrawingPreview = FALSE;
int CurrentShapeIndex = line;
int g_left;
int g_top;
int g_right;
int g_bottom;
int* g_dataFromFile;
int g_sizeOfData = 0;

RECT canvasRect;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
		return FALSE;

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;

	// Initialize GDI+.
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_SIMPLEPAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SIMPLEPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	CoUninitialize();
	GdiplusShutdown(gdiplusToken);

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = 0 /*CS_HREDRAW | CS_VREDRAW*/;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SIMPLEPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_SIMPLEPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   g_hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!g_hWnd)
   {
      return FALSE;
   }

   ShowWindow(g_hWnd, nCmdShow);
   UpdateWindow(g_hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	g_ShapeModels.push_back(new CLine);
	g_ShapeModels.push_back(new CRectangle);
	g_ShapeModels.push_back(new CSquare);
	g_ShapeModels.push_back(new CEllipse);
	g_ShapeModels.push_back(new CCircle);

	bool initSuccess = InitializeFramework(hWnd);
	if (!initSuccess)
		return -1;

	GetWindowRect(hWnd, &canvasRect);
	canvasRect.top = 148;
	canvasRect.left = 0;

	return TRUE;
}

typedef void(_cdecl *MYPROC)(int* &, int &, OPENFILENAME&);

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	HINSTANCE hinstLib;
	MYPROC ProcAdd;

	hinstLib = LoadLibrary(L"SaveLoad.dll");

	HMENU hMenu = GetMenu(hWnd);
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	// TODO: complete code to control menu
	case ID_SHAPE_LINE:
	{
		CurrentShapeIndex = line;
	}
		break;
	case ID_SHAPE_RECTANGLE:
	{
		CurrentShapeIndex = rectangle;
	}
		break;
	case ID_SHAPE_ELLIPSE:
	{
		CurrentShapeIndex = ellipse;
	}
		break;
	case ID_FILE_OPEN:
	{
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = BUFFER_SIZE;
		ofn.lpstrFilter = L"My paint data (*.mpd) \0*.mpd\0All files (*.*) \0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		if (GetOpenFileName(&ofn))
		{
			if (hinstLib)
			{
				ProcAdd = (MYPROC)GetProcAddress(hinstLib, "OpenPictureData");
				if (ProcAdd != NULL)
				{
					(ProcAdd)(g_dataFromFile, g_sizeOfData, ofn);
				}
				else
				{
					g_dataFromFile = NULL;
					g_sizeOfData = 0;
				}
			}
			FromFileToShape(g_Shapes, g_dataFromFile, g_sizeOfData, hWnd);
		}
	}
		break;
	case ID_FILE_SAVE:
	{
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = hWnd;
		ofn.nMaxFile = BUFFER_SIZE;
		ofn.lpstrFile = szFile;
		ofn.lpstrDefExt = L"mpd";
		ofn.lpstrFilter = L"My paint data (*.mpd) \0*.mpd\0All files (*.*) \0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		if (GetSaveFileName(&ofn))
		{
			FromShapeToFile(g_Shapes, g_dataFromFile, g_sizeOfData);
			if (hinstLib)
			{
				ProcAdd = (MYPROC)GetProcAddress(hinstLib, "SavePictureData");
				if (ProcAdd != NULL)
				{
					(ProcAdd)(g_dataFromFile, g_sizeOfData, ofn);
				}
			}
		}
	}
		break;

	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	for (unsigned int i = 0; i < g_Shapes.size(); i++)
	{
		g_Shapes[i]->Draw(hWnd);
	}
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}

// TODO: complete code to control mouse
void OnLButtonDown(HWND hWnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	IsDrawingPreview = TRUE;
	g_Shapes.push_back(g_ShapeModels[CurrentShapeIndex]->CreateShape());
	g_left = x;
	g_top = y;
}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags)
{
	if (IsDrawingPreview)
	{
		if (CurrentShapeIndex == rectangle && keyFlags & MK_SHIFT)
		{
			CurrentShapeIndex = square;
			g_Shapes.pop_back();
			g_Shapes.push_back(g_ShapeModels[CurrentShapeIndex]->CreateShape());
		}

		if (CurrentShapeIndex == square && (keyFlags & MK_SHIFT) == 0)
		{
			CurrentShapeIndex = rectangle;
			g_Shapes.pop_back();
			g_Shapes.push_back(g_ShapeModels[CurrentShapeIndex]->CreateShape());
		}

		if (CurrentShapeIndex == ellipse && keyFlags & MK_SHIFT)
		{
			CurrentShapeIndex = circle;
			g_Shapes.pop_back();
			g_Shapes.push_back(g_ShapeModels[CurrentShapeIndex]->CreateShape());
		}

		if (CurrentShapeIndex == circle && (keyFlags & MK_SHIFT) == 0)
		{
			CurrentShapeIndex = ellipse;
			g_Shapes.pop_back();
			g_Shapes.push_back(g_ShapeModels[CurrentShapeIndex]->CreateShape());
		}

		g_right = x;
		g_bottom = y;

		g_Shapes.back()->Draw(hWnd, g_left, g_top, g_right, g_bottom);
		InvalidateRect(hWnd, &canvasRect, TRUE);
	}
}

void OnLButtonUp(HWND hWnd, int x, int y, UINT keyFlags)
{
	g_right = x;
	g_bottom = y;

	g_Shapes.back()->Draw(hWnd, g_left, g_top, g_right, g_bottom);
	IsDrawingPreview = FALSE;
	InvalidateRect(hWnd, &canvasRect, TRUE);
}


void FromFileToShape(vector<CShape*> &shapes, int* data, int sizeOfData, HWND hWnd)
{
	if (sizeOfData == 0)
		return;

	unsigned int countElement = sizeOfData / sizeof(int);
	unsigned int i = 0;
	g_Shapes.clear();
	while (i < countElement)
	{
		g_Shapes.push_back(g_ShapeModels[data[i]]->CreateShape());
		g_Shapes.back()->Draw(hWnd, data[i + 1], data[i + 2], data[i + 3], data[i + 4]);
		i += 5;
	}
	InvalidateRect(hWnd, &canvasRect, TRUE);
}

void FromShapeToFile(vector<CShape*> shapes, int* &data, int &sizeOfData)
{
	size_t countShape = shapes.size();
	sizeOfData = countShape * 5 * sizeof(int);
	data = new int[countShape * 5];

	int dataIndex = 0;

	for (unsigned int i = 0; i < countShape; i++)
	{
		data[dataIndex] = shapes[i]->GetType();
		dataIndex++;
		data[dataIndex] = shapes[i]->GetLeft();
		dataIndex++;
		data[dataIndex] = shapes[i]->GetTop();
		dataIndex++;
		data[dataIndex] = shapes[i]->GetRight();
		dataIndex++;
		data[dataIndex] = shapes[i]->GetBottom();
		dataIndex++;
	}
}