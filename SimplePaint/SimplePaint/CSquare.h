#pragma once
#include "CShape.h"
class CSquare :
	public CShape
{
public:
	void Draw(HWND hWnd, int left, int top, int right, int bottom);
	CShape* CreateShape();
	void Draw(HWND hWnd);
	int GetType();
};