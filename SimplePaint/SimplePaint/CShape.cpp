#include "stdafx.h"
#include "CShape.h"

int CShape::GetLeft()
{
	return this->left;
}

int CShape::GetTop()
{
	return this->top;
}

int CShape::GetRight()
{
	return this->right;
}

int CShape::GetBottom()
{
	return this->bottom;
}