#include "stdafx.h"
#include "CLine.h"

void CLine::Draw(HWND hWnd, int left, int top, int right, int bottom)
{
	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;

	HDC hdc = GetDC(hWnd);

	Graphics* graphics = new Graphics(hdc);
	Pen* pen = new Pen(Color(255, 0, 0, 0));
	graphics->DrawLine(pen, left, top, right, bottom);
	delete pen;
	delete graphics;

	ReleaseDC(hWnd, hdc);
}

CShape* CLine::CreateShape()
{
	CShape* shape = new CLine;
	return shape;
}

void CLine::Draw(HWND hWnd)
{
	this->Draw(hWnd, left, top, right, bottom);
}

int CLine::GetType()
{
	return line;
}