#include "stdafx.h"
#include "CEllipse.h"

void CEllipse::Draw(HWND hWnd, int left, int top, int right, int bottom)
{
	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;

	HDC hdc = GetDC(hWnd);
	Graphics* graphics = new Graphics(hdc);
	Pen* pen = new Pen(Color(255, 0, 0, 0));
	graphics->DrawEllipse(pen, left, top, right - left, bottom - top);
	delete pen;
	delete graphics;

	ReleaseDC(hWnd, hdc);
}

CShape* CEllipse::CreateShape()
{
	CShape* shape = new CEllipse;
	return shape;
}

void CEllipse::Draw(HWND hWnd)
{
	this->Draw(hWnd, left, top, right, bottom);
}

int CEllipse::GetType()
{
	return ellipse;
}