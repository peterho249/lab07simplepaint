#pragma once
class CShape
{
protected:
	int left;
	int top;
	int right;
	int bottom;
public:
	virtual void Draw(HWND hWnd, int left, int top, int right, int bottom) = 0;
	virtual CShape* CreateShape() = 0;
	virtual void Draw(HWND hWnd) = 0;
	virtual int GetType() = 0;
	int GetLeft();
	int GetTop();
	int GetRight();
	int GetBottom();
};

