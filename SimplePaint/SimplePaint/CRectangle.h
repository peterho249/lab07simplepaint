#pragma once
#include "CShape.h"
class CRectangle :
	public CShape
{
public:
	void Draw(HWND hWnd, int left, int top, int right, int bottom);
	CShape* CreateShape();
	void Draw(HWND hWnd);
	int GetType();
};

