#include "stdafx.h"
#include "CRectangle.h"

void CRectangle::Draw(HWND hWnd, int left, int top, int right, int bottom)
{
	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;

	if (right < left)
	{
		int temp = right;
		right = left;
		left = temp;
	}

	if (bottom < top)
	{
		int temp = top;
		top = bottom;
		bottom = temp;
	}

	HDC hdc = GetDC(hWnd);
	Graphics* graphics = new Graphics(hdc);
	Pen* pen = new Pen(Color(255, 0, 0, 0));
	graphics->DrawRectangle(pen, left, top, right - left, bottom - top);
	delete pen;
	delete graphics;

	ReleaseDC(hWnd, hdc);
}

CShape* CRectangle::CreateShape()
{
	CShape* shape = new CRectangle;
	return shape;
}

void CRectangle::Draw(HWND hWnd)
{
	this->Draw(hWnd, left, top, right, bottom);
}

int CRectangle::GetType()
{
	return rectangle;
}