#include "stdafx.h"
#include "CSquare.h"

void CSquare::Draw(HWND hWnd, int left, int top, int right, int bottom)
{
	int temp = (abs(right - left) + abs(bottom - top)) / 2;

	if (left > right)
		right = left - temp;
	else
		right = left + temp;

	if (top > bottom)
		bottom = top - temp;
	else
		bottom = top + temp;

	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;

	if (right < left)
	{
		int temp = right;
		right = left;
		left = temp;
	}

	if (bottom < top)
	{
		int temp = top;
		top = bottom;
		bottom = temp;
	}

	HDC hdc = GetDC(hWnd);
	Graphics* graphics = new Graphics(hdc);
	Pen* pen = new Pen(Color(255, 0, 0, 0));
	graphics->DrawRectangle(pen, left, top, right - left, bottom - top);
	delete pen;
	delete graphics;

	ReleaseDC(hWnd, hdc);
}

CShape* CSquare::CreateShape()
{
	CShape* shape = new CSquare;
	return shape;
}

void CSquare::Draw(HWND hWnd)
{
	this->Draw(hWnd, left, top, right, bottom);
}

int CSquare::GetType()
{
	return square;
}