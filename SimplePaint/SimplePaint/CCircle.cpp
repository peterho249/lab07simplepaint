#include "stdafx.h"
#include "CCircle.h"

void CCircle::Draw(HWND hWnd, int left, int top, int right, int bottom)
{
	int temp = (abs(right - left) + abs(bottom - top)) / 2;

	if (left > right)
		right = left - temp;
	else
		right = left + temp;

	if (top > bottom)
		bottom = top - temp;
	else
		bottom = top + temp;

	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;

	HDC hdc = GetDC(hWnd);
	
	Graphics* graphics = new Graphics(hdc);
	Pen* pen = new Pen(Color(255, 0, 0, 0));
	graphics->DrawEllipse(pen, left, top, right - left, bottom - top);
	delete pen;
	delete graphics;

	ReleaseDC(hWnd, hdc);
}

CShape* CCircle::CreateShape()
{
	CShape* shape = new CCircle;
	return shape;
}

void CCircle::Draw(HWND hWnd)
{
	this->Draw(hWnd, left, top, right, bottom);
}

int CCircle::GetType()
{
	return circle;
}