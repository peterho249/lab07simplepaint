// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <WinUser.h>
#include <commdlg.h>
#include <fstream>
#include <Wingdi.h>


// TODO: reference additional headers your program requires here
#include <windowsx.h>
#include <vector>
using namespace std;

#include "CShape.h"
#include "CLine.h"
#include "CRectangle.h"
#include "CEllipse.h"
#include "CSquare.h"
#include "CCircle.h"

#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"

#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;

#include "..\\SaveLoad\\SaveLoad.h"
#pragma comment(lib, "..\\x64\\Debug\\SaveLoad.lib")

#pragma comment(lib, "Gdi32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

enum ShapeIndex						// Chi so mau hinh
{
	line,
	rectangle,
	square,
	ellipse,
	circle
};

