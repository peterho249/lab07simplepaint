// SaveLoad.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <fstream>
using namespace std;

void OpenPictureData(int* &data, int &sizeOfData, OPENFILENAME &ofn)
{
	ifstream inFile;
	inFile.open(ofn.lpstrFile, ios::binary);
	inFile.seekg(0, ios::end);
	size_t fileSize = inFile.tellg();
	inFile.seekg(0, ios::beg);

	data = new int[fileSize / sizeof(int)];
	sizeOfData = fileSize;

	inFile.read((char*)data, fileSize);
	inFile.close();
}

void SavePictureData(int* &data, int &sizeOfData, OPENFILENAME &ofn)
{
	ofstream outFile;
	outFile.open(ofn.lpstrFile, ios::binary);

	outFile.write((char*)data, sizeOfData);
	outFile.close();
}
