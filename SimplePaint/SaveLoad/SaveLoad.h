#pragma once
#include <commdlg.h>

#ifndef SAVELOAD_H
#define SAVELOADCLASS __declspec(dllexport)
#else
#define SAVELOADCLASS __declspec(dllimport)
#endif

// Save and load binary file
extern "C" SAVELOADCLASS void OpenPictureData(int* &data, int &sizeOfData, OPENFILENAME &ofn);
extern "C" SAVELOADCLASS void SavePictureData(int* &data, int &sizeOfData, OPENFILENAME &ofn);